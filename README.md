# README #

Spring Boot Microservice (Business Service)

### What is this repository for? ###

* Example business service (registration) to be used with [Eureka Server](https://bitbucket.org/davidjbarnes/eureka-server-spring-boot) and [data service (user)](https://bitbucket.org/davidjbarnes/spring-boot-data-service/src/7ee28556d102e851f3f18fea18db3dae46eef72c?at=master) projects.
* 0.0.1

### How do I get set up? ###

* $ git clone
* $ cd registration-service-bs
* $ mv spring-boot:run

### Who do I talk to? ###

* David J Barnes
* www.david-barnes.com