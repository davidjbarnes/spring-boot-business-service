package com.barnes.business.registration;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.barnes.business.registration.domain.User;
import com.barnes.business.registration.repository.UserRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;

@RestController
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;
    
    @RequestMapping("/test")
	public String test() {
		return userRepository.test();
	}
    
    @RequestMapping("/getUser")
	public User getUser() {
		return userRepository.getUser();
	}
    
    @RequestMapping("/listAll")
	public List<User> listAll() {		
		return userRepository.listAll();
	}
}


