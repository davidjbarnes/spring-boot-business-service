package com.barnes.business.registration.repository;

import com.barnes.business.registration.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@Repository
public class UserRepository
{
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
    @Autowired
    RestTemplate restTemplate;

    public String test()
    {
        return restTemplate.getForObject( "http://user-ds/test", String.class );
    }
    
    public User getUser()
    {
        return restTemplate.getForObject( "http://user-ds/{id}", User.class, "6c84fb90-12c4-11e1-840d-7b25c5ee775a" );
    }
    
    @SuppressWarnings("unchecked")
	public List<User> listAll()
    {
        return restTemplate.getForObject( "http://user-ds/listAll", List.class );
    }
}
